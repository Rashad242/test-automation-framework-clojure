(defproject automation-framework "0.1.0-SNAPSHOT"
  :description "BCCS242: Test Automation"
  :url "http://admin.bccs242.com/"
  :min-lein-version "2.0.0"
  :dependencies [[org.clojure/clojure "1.7.0"]
                 ;[compojure "1.3.1"]
                 ;[ring "1.1.6"]
                 ;[ring/ring-defaults "0.1.2"]
                 [org.clojure/data.json "0.2.5"]
                 [clj-webdriver "0.7.2"]
                 [org.seleniumhq.selenium/selenium-server "2.47.0"]
                 ]
  ;:plugins [[lein-ring "0.8.13"]]
  ;:ring {:handler clj-webdriver-tutorial.handler/app}

  )
