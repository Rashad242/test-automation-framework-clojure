(ns automation-framework.base-test
  (:require [clojure.test :refer :all]
            ;[ring.adapter.jetty :refer [run-jetty]]
            [clj-webdriver.taxi :refer :all]
            [clojure.data.json :as json]))

(def config-json "test/automation_framework/config/config.json")

(def good-username "owner1")

(def good-password "energy")

(def bad-username "neg-cred1")

(def bad-password "badpass")

(defn modify-keys [f m] (zipmap (map f (keys m)) (vals m)))

(defn get-element-from-json [in-path & [element-name]]
  (let [json-file (modify-keys keyword (json/read-str (slurp in-path)))
        child-of (modify-keys keyword (:child-of (modify-keys keyword (element-name (modify-keys keyword json-file)))))
        path (:path (modify-keys keyword child-of))
        parent-name (:element-name (modify-keys keyword child-of))
        strategy (modify-keys keyword (:strategy (modify-keys keyword (element-name (modify-keys keyword json-file)))))]
    (cond
      (= path "nil") (find-element strategy)
      (= path ".") (find-element-under (get-element-from-json in-path (keyword parent-name)) strategy))))

(def test-config (modify-keys keyword (json/read-str (slurp config-json))))

(defn get-unique-number []
  (subs (str (System/currentTimeMillis)) 6 13))

(def unique-test-number (get-unique-number))

(defn page-screenshot [in-name]
  (Thread/sleep 500)
  (take-screenshot :file in-name))

(defn start-browser []
  (set-driver! {:browser :firefox} (:base-url test-config))
  (window-maximize))

(defn stop-browser []
  (quit))

                                        ;(start-browser)
                                        ;(stop-browser)
