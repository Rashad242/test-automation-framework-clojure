(ns automation-framework.dashboard.dashboard
  (:require [clojure.test :refer :all]
            [clj-webdriver.taxi :refer :all]
            [automation-framework.base-test :refer :all]))

(def dashboard-json "test/automation_framework/dashboard/dashboard.json")

(defn debtors-link [] (get-element-from-json dashboard-json :debtors-link))
