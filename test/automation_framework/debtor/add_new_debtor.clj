(ns automation-framework.debtor.add-new-debtor
  (:require [clojure.test :refer :all]
            [clj-webdriver.taxi :refer :all]
            [automation-framework.base-test :refer :all]
            [automation-framework.homepage.homepage :refer :all]
            [automation-framework.debtor.debtor :refer :all]
            [automation-framework.landing.landing :refer :all]))

(def add-new-debtor-json "test/automation_framework/debtor/add_new_debtor.json")

(defn addDebtorContainer [] (get-element-from-json add-new-debtor-json :addDebtorContainer))

(defn txt_company [] (get-element-from-json add-new-debtor-json :txt_company))

(defn cmbox_salutation [] (get-element-from-json add-new-debtor-json :cmbox_salutation))

(defn txt_first_name [] (get-element-from-json add-new-debtor-json :txt_first_name))

(defn txt_last_name [] (get-element-from-json add-new-debtor-json :txt_last_name))

(defn txt_dob [] (get-element-from-json add-new-debtor-json :txt_dob))

(defn txt_email [] (get-element-from-json add-new-debtor-json :txt_email))

(defn txt_nib_number [] (get-element-from-json add-new-debtor-json :txt_nib_number))

(defn txt_dl_number [] (get-element-from-json add-new-debtor-json :txt_dl_number))

(defn txt_account_number [] (get-element-from-json add-new-debtor-json :txt_account_number))

(defn body [] (get-element-from-json add-new-debtor-json :body))

(defn btn_save [] (get-element-from-json add-new-debtor-json :btn_save))

(defn btn-done [] (get-element-from-json add-new-debtor-json :btn_done))

(defn add-debtor-form [in-company in-salutation in-first-name in-last-name in-dob in-email in-nib-number in-dl-number in-account-number]
  (input-text (txt_company) in-company)
  (select-option (cmbox_salutation) {:value in-salutation})
  (input-text (txt_first_name) in-first-name)
  (input-text (txt_last_name) in-last-name)
  (input-text (txt_dob) in-dob)
  (click (btn-done))
  (input-text (txt_email) in-email)
  (input-text (txt_nib_number) in-email)
  (input-text (txt_nib_number) in-nib-number)
  (input-text (txt_dl_number) in-dl-number)
  (input-text (txt_account_number) in-account-number)
  (click (btn_save)))

(deftest verify-add-new-debtor-page []
  (start-browser)
  (login good-username good-password)
  (click (view-all-debtor-link))
  (click (quick-add-debtor))
  (is (= true (visible? (addDebtorContainer))))
  (is (= true (visible? (txt_company))))
  (is (= true (visible? (cmbox_salutation))))
  (is (= true (visible? (txt_first_name))))
  (is (= true (visible? (txt_last_name))))
  (is (= true (visible? (txt_dob))))
  (is (= true (visible? (txt_email))))
  (is (= true (visible? (txt_nib_number))))
  (is (= true (visible? (txt_dl_number))))
  (is (= true (visible? (txt_account_number))))
  (is (= true (visible? (body))))
  (is (= true (visible? (btn_save))))
  (stop-browser))

(deftest add-new-debtor []
  (start-browser)
  (login good-username good-password)
  (click (view-all-debtor-link))
  (click (quick-add-debtor))
  (add-debtor-form "ESPN" "Mr." (str "Bill-" unique-test-number) (str "Clinton-" unique-test-number) "12/07/1986" "white@house.gov" "12345" "56789" "012345")
  (stop-browser))
