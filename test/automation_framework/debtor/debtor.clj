(ns automation-framework.debtor.debtor
  (:require [clojure.test :refer :all]
            [clj-webdriver.taxi :refer :all]
            [automation-framework.base-test :refer :all]
            [automation-framework.dashboard.dashboard :refer :all]
            [automation-framework.landing.landing :refer :all]
            [automation-framework.homepage.homepage :refer :all]
            ;[automation-framework.debtor.add-new-debtor :refer :all]
            ))

(def debtor-grid-columns '("Debtor id" "Last name" "Salutation" "Name" "Nib" "Driverno" "Dob" "Date created" "Actions"))

(def debtor-json "test/automation_framework/debtor/debtor.json")

(defn quick-add-debtor [] (get-element-from-json debtor-json :quick-add-debtor))

(defn quick-add-user [] (get-element-from-json debtor-json :quick-add-user))

(defn view-by-account [] (get-element-from-json debtor-json :view-by-account))

(defn search-field [] (get-element-from-json debtor-json :search-field))

(defn search-text [] (get-element-from-json debtor-json :search-text))

(defn btn-search [] (get-element-from-json debtor-json :btn-search))

(defn results-table [] (get-element-from-json debtor-json :results-table))

(defn results-header [] (get-element-from-json debtor-json :results-header))

(defn results-header-rows [] (get-element-from-json debtor-json :results-header-rows))

(defn columns [] (find-elements-under (results-header-rows) {:tag "th"}))

(defn results-body [] (get-element-from-json debtor-json :results-body))

(defn rows [] (find-elements-under (results-body) {:tag "tr"}))

(defn convert-row [in-row in-columns]
  (apply hash-map (interleave
                  (map #(keyword (attribute (find-element % {:tag "div"}) :rel)) in-columns)
                  (map #(text %) (find-elements-under in-row {:tag "td"})))))

(defn get-table-data []
  (map #(convert-row % (columns)) (rows)))

(defn debtor-search [in-search in-search-by]
  (input-text (search-text) in-search)
  (select-option (search-field) in-search-by))

(deftest verify-debtor-page []
  (start-browser)
  (login good-username good-password)
  (click (view-all-debtor-link))
  (is (= true (visible? (quick-add-debtor))))
  (is (= true (visible? (quick-add-user))))
  (is (= true (visible? (view-by-account))))
  (is (= true (visible? (search-field))))
  (is (= true (visible? (search-text))))
  (is (= true (visible? (btn-search))))
  (is (= true (visible? (results-table))))
  (is (= true (visible? (results-header))))
  (is (= true (visible? (results-header-rows))))
  (is (= true (visible? (results-body))))
  (map #(is (= true (.contains debtor-grid-columns (text %)))) (columns))
  (stop-browser))

(defn exists-in-table? [in-key in-value in-table-data]
  (if (> (count (filter (fn [x] (= (in-key x) in-value)) in-table-data)) 0) true false))

(defn get-row [in-key in-value in-table-data]
  (first (filter (fn [x] (= (in-key x) in-value)) in-table-data)))

(deftest debtor-search-by-id []
  (start-browser)
  (login good-username good-password)
  (click (view-all-debtor-link))
  (input-text (search-text) "36")
  (select-option (search-field) {:value "debtor_id"})
  (click (btn-search))
  (Thread/sleep 1000)
  (is (= (count (rows)) 1))
  (let [test-data (get-row :debtor_id "36" (get-table-data))]
    (is (= (:driverno test-data) "23456"))
    (is (= (:nib test-data) "1234567"))
    (is (= (:name test-data) "Michael"))
    (is (= (:debtor_id test-data) "36"))
    (is (= (:salutation test-data) "Mr."))
    (is (= (:dob test-data) "08/11/2015"))
    (is (= (:last_name test-data) "Forbes"))
    (is (= (:date_created test-data) "02/11/2015 - 19:50")))
  (stop-browser))
