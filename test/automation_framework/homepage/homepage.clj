(ns automation-framework.homepage.homepage
  (:require [clojure.test :refer :all]
            [clj-webdriver.taxi :refer :all]
            [automation-framework.base-test :refer :all]))

(def homepage-json "test/automation_framework/homepage/homepage.json")

(defn list-accounts-dropdown [] (get-element-from-json homepage-json :list-accounts-dropdown))

(defn clients-link [] (get-element-from-json homepage-json :clients-link))

(defn view-all-debtor-link [] (get-element-from-json homepage-json :view-all-debtor-link))

(defn debtors-object [] (get-element-from-json homepage-json :debtors-object))

;(click (clients-link))

;(select (list-accounts-dropdown) (clients-link))

;(move-to-element (list-accounts-dropdown))

;(visible? (view-all-debtor-link))

;(visible? (debtors-object))
