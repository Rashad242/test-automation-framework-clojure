(ns automation-framework.landing.landing
  (:require [clojure.test :refer :all]
            [clj-webdriver.taxi :refer :all]
            [automation-framework.base-test :refer :all]))

(def landing-json "test/automation_framework/landing/landing.json")

(defn txt-login-username [] (get-element-from-json landing-json :login_username))

(defn txt-login-password [] (get-element-from-json landing-json :login_password))

(defn btn-submit [] (get-element-from-json landing-json :submit))

(defn main-container [] (get-element-from-json landing-json :main-container))

(defn header-content [] (get-element-from-json landing-json :header-content))

(defn main-content [] (get-element-from-json landing-json :main-content))

(defn footer-content [] (get-element-from-json landing-json :footer-content))

(defn logged-in-container [] (get-element-from-json landing-json :logged-in-container))

(defn error-box [] (get-element-from-json landing-json :error-box))

(defn login [username password]
  (input-text (txt-login-username) username)
  (input-text (txt-login-password) password)
  (click (btn-submit)))

(deftest verify-landing-page-load []
  (start-browser)
  (is (= true (visible? (main-container))))
  (is (= true (visible? (footer-content))))
  (is (= true (visible? (main-content))))
  (is (= true (visible? (header-content))))
  (is (= true (visible? (txt-login-username))))
  (is (= true (visible? (txt-login-password))))
  (is (= true (visible? (btn-submit))))
  (stop-browser))

(deftest negative-login
  (start-browser)
  (login bad-username bad-password)
  (is (= true (visible? (error-box))))
  (is (= true (visible? (main-container))))
  (is (= true (visible? (header-content))))
  (is (= true (visible? (main-content))))
  (is (= true (visible? (footer-content))))
  (stop-browser))

(deftest positive-login
  (start-browser)
  (login good-username good-password)
  (stop-browser))
